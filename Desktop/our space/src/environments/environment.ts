// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  groupID:"55783583",
  privateToken:"private_token=glpat-Z_sheX8m8BHzHZG8sLBV",
  accessdataPath:"config%2Ejson",
  indexdeliveriesPath:"indexDeliveries%2Ecsv",
  indexhotfixesPath:"indexHotfixes%2Ecsv",
  indexreleasesPath:"indexReleases%2Ecsv",
  readmePath:"README%2Emd",
  toolsPath:"assets/json/tools.json"
};
