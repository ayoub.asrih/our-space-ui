export const environment = {
  production: true,
  groupID:"55783583",
  privateToken:"private_token=glpat-Z_sheX8m8BHzHZG8sLBV",
  accessdataPath:"config%2Ejson",
  indexdeliveriesPath:"indexDeliveries%2Ecsv",
  indexhotfixesPath:"indexHotfixes%2Ecsv",
  indexreleasesPath:"indexReleases%2Ecsv",
  readmePath:"README%2Emd",
  toolsPath:"assets/json/tools.json"
};
