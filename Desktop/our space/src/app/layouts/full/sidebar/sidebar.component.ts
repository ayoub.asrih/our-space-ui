import { ChangeDetectorRef, Component, Injectable, OnDestroy, OnInit } from '@angular/core';
import { MediaMatcher } from '@angular/cdk/layout';
import { MenuItems } from '../../../shared/menu-items/menu-items';
import {BehaviorSubject, forkJoin, map, Observable, of as observableOf} from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { DataService } from './data.service';
import { Data } from './data'



 @Injectable({
   providedIn: 'root' })
@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class AppSidebarComponent implements OnDestroy,OnInit {
  mobileQuery: MediaQueryList;
  private _mobileQueryListener: () => void;

  
  result: Data[] = [];


  constructor(
    changeDetectorRef: ChangeDetectorRef,
    media: MediaMatcher,
    public menuItems: MenuItems,
    public http: HttpClient,
    public dataService: DataService
  ) {
    this.mobileQuery = media.matchMedia('(min-width: 768px)');
    this._mobileQueryListener = () => changeDetectorRef.detectChanges();
    this.mobileQuery.addListener(this._mobileQueryListener);

     this.getRoots();  
     console.log(this.result);
  }


  ngOnInit(): void {
  }

  getRoots()
  {
    this.dataService.getRoot().subscribe(response => {
      this.result =  response
      this.result.forEach(directory => {
        let nodeList: Data[]= []
        this.dataService.getNodes(directory.id!).subscribe((response) => {
          nodeList = response
          if(nodeList.length>0)
          {   
            nodeList.forEach(node=>{
              let nodes: Data[]=[]   
              this.dataService.getNodes(node.id!).subscribe((res:any)=>{
                nodes = <Data[]> res
                if(nodes.length>0)
                {
                  nodes.forEach(child=>
                    {
                      let children: Data[]=[]
                      this.dataService.getNodes(child.id!).subscribe((result:any)=>
                      {
                        children = <Data[]> result
                        if(children.length>0)
                          child.children = result;
                      })
                    })
                }
                  node.children = nodes;
                nodes = [];
              })
            })
            directory.children = nodeList;
          }
          nodeList = [];
        })    
      });
    })
  }
  refresh()
  {
    window.location.reload();
  }
  ngOnDestroy(): void {
    this.mobileQuery.removeListener(this._mobileQueryListener);
  }
  hasChild = (_: number, node: Data) => !!node.children && node.children.length>0;
}
