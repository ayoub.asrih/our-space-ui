import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ColumnMode } from '@swimlane/ngx-datatable';

@Component({
  selector: 'app-documents',
  templateUrl: './documents.component.html',
  styleUrls: ['./documents.component.css']
})
export class DocumentsComponent implements OnInit {
  reorderable = true;
  ColumnMode = ColumnMode;
  data:any = [];
  constructor(private http: HttpClient) { }

  ngOnInit(): void {
    this.getData();
  }
  getData()
  {
    this.http.get("assets/json/documents.json").subscribe((res:any) => {
      this.data = res;
      console.log(this.data);
    })
  }

}
