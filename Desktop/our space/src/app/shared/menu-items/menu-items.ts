import { Injectable } from '@angular/core';
import { DataService } from 'app/layouts/full/sidebar/data.service';

export interface Menu {
  name: string;
}

var MENUITEMS: Menu[];

@Injectable({
  providedIn: 'root' })
export class MenuItems {



  constructor(private dataService: DataService){
    this.dataService.getRoot().subscribe(response =>{
      MENUITEMS = response;

    }
    )
   
  } 


  getMenuitem(): Menu[] {
    
    return MENUITEMS;
  }
}
