import { BrowserModule } from '@angular/platform-browser';
import { LOCALE_ID, NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { LocationStrategy, PathLocationStrategy } from '@angular/common';
import { AppRoutes } from './app.routing';
import { AppComponent } from './app.component';

import { FlexLayoutModule } from '@angular/flex-layout';
import { FullComponent } from './layouts/full/full.component';
import { AppHeaderComponent } from './layouts/full/header/header.component';
import { AppSidebarComponent } from './layouts/full/sidebar/sidebar.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DemoMaterialModule } from './demo-material-module';
import { MatNestedTreeNode, MatTree, MatTreeModule } from '@angular/material/tree';
import { SharedModule } from './shared/shared.module';
import { SpinnerComponent } from './shared/spinner.component';
import { TreeModule } from 'primeng/tree';
import { ButtonModule } from 'primeng/button';
import { GroupDetailComponent } from './group-detail/group-detail.component';
import { MarkdownModule } from 'ngx-markdown';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { ProjectDetailComponent } from './project-detail/project-detail.component';
import { MatButtonModule } from '@angular/material/button';
import { ToolsComponent } from './tools/tools.component';
import { IgxTreeModule } from 'igniteui-angular';
import { LivraisonComponent } from './livraison/livraison.component';
import { AccessesComponent } from './accesses/accesses.component';
import '@angular/common/locales/global/fr';
import { LienUtileComponent } from './lien-utile/lien-utile.component';
import { DocumentsComponent } from './documents/documents.component';

@NgModule({
  declarations: [
    AppComponent,
    FullComponent,
    AppHeaderComponent,
    SpinnerComponent,
    AppSidebarComponent,
    GroupDetailComponent,
    ProjectDetailComponent,
    ToolsComponent,
    LivraisonComponent,
    AccessesComponent,
    LienUtileComponent,
    DocumentsComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    DemoMaterialModule,
    FormsModule,
    FlexLayoutModule,
    HttpClientModule,
    SharedModule,
    MatTreeModule,
    TreeModule,
    ButtonModule,
    MatButtonModule,
    RouterModule.forRoot(AppRoutes),
    MarkdownModule.forRoot(),
    NgxDatatableModule,
    IgxTreeModule
  ],
  providers: [
    { provide: LOCALE_ID, useValue: "fr-FR" },
    {
      provide: LocationStrategy,
      useClass: PathLocationStrategy
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
