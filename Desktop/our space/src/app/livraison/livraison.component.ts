import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { environment } from 'environments/environment';
@Component({
  selector: 'app-livraison',
  templateUrl: './livraison.component.html',
  styleUrls: ['./livraison.component.css']
})
export class LivraisonComponent implements OnInit {

  indexText: string ='';
  indexValues: string[]=[];
  indexHeaders: any = [];
  indexRows: any=[];

  constructor(private http:HttpClient) { }

  ngOnInit(): void {
    this.getIndexContent();
  }
  getIndexContent()
	{
		
		this.http.get(`https://gitlab.com/api/v4/projects/37965907/repository/files/${environment.indexdeliveriesPath}?ref=main&${environment.privateToken}`)
		.subscribe((data:any) =>
		{
			this.indexText = data.content;			
			this.indexText = atob(this.indexText);
			console.log(this.indexText);
			this.indexValues = this.indexText.split("\n");
			this.indexHeaders = this.indexValues[1];
			this.indexRows = this.indexValues[0];
			this.indexHeaders = this.indexHeaders.split(",");
			this.indexRows = this.indexRows.split(",");
			this.indexRows.forEach((element: string, index:number) => {
				this.indexRows[index] = element.replace(/"/g, "");	
			});
			
		})
	}
}
