import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ColumnMode } from '@swimlane/ngx-datatable';
import { DataService } from 'app/layouts/full/sidebar/data.service';
@Component({
  selector: 'app-group-detail',
  templateUrl: './group-detail.component.html',
  styleUrls: ['./group-detail.component.css']
})
export class GroupDetailComponent implements OnInit {


	ColumnMode = ColumnMode;
	loadingIndicator = true;
	reorderable = true;
	columns = [{ name: 'Nom du projet', prop: 'name' },{ name: 'Actions', prop: 'id' },{name:'Date de création', prop: 'created_at'}];
  	queryId = ''
  	bool: boolean =false;
	selectedValue: any = '';
	list: any =[]
	path: any=[];
	chemin: string ='';
	groupRows:any =[];
	projectRows:any =[];
	value:any;
	constructor(private http: HttpClient, private activatedRouter: ActivatedRoute, private router: Router, private dataService:DataService){
    this.activatedRouter.params.subscribe(params=> 
      this.queryId = params.id
      )
      this.router.routeReuseStrategy.shouldReuseRoute = function() {
        return false;
      };
  }
	ngOnInit(): void {
		
		this.getProjects();
		this.getGroupsPath();
	}
	ngAfterViewInit() { }
	
	getGroupsPath()
	{
		this.http.get(this.dataService.getGroupEndpoint(this.queryId)).subscribe((data:any) =>{
			this.path.push(data.name);
			this.http.get(this.dataService.getGroupEndpoint(data.parent_id)).subscribe((res:any) =>{
				this.path.push(res.name);
				console.log(this.path);
				this.path = this.path.reverse();
					this.chemin = this.path.join(' > ');
				if(res.parent_id)
				{
				this.http.get(this.dataService.getGroupEndpoint(res.parent_id)).subscribe((result:any) =>{
					this.path = this.path.reverse();
					this.path.push(result.name)
					console.log(this.path);
					this.path = this.path.reverse();
					this.chemin = this.path.join(' > ');
					
					if(result.parent_id)
					{
					this.http.get(this.dataService.getGroupEndpoint(result.parent_id)).subscribe((resu:any) =>{
						
						this.path = this.path.reverse();
						this.path.push(resu.name)
						console.log(this.path);
						this.path = this.path.reverse();
						this.chemin = this.path.join(' > ');
						
					})
					}
					
				})
				}
				
				
			})
			
		})
	}
	getProjects()
	{
		this.http.get(this.dataService.getProjectEndpoint(this.queryId)).subscribe(data =>{
			 this.projectRows = data; 
  /* 			this.rows = this.list.map((res:any) =>({name: res.name, id: res.id})) */
			
	})
	}

}
