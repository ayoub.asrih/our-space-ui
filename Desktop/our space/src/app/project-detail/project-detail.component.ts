import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { environment } from 'environments/environment';
import { DataService } from 'app/layouts/full/sidebar/data.service';


@Component({
  selector: 'app-project-detail',
  templateUrl: './project-detail.component.html',
  styleUrls: ['./project-detail.component.css']
})
export class ProjectDetailComponent implements OnInit {

  queryId = '';
  delError = "";
  hotfixError = "";
  relError = "";
  bool: boolean =false;
  readmeText: string = '';
  indexText: string ='';
  indexValues: string[]=[];
  indexHeaders: any = [];
  indexRows: any=[];
  hotfixText: string ='';
  hotfixValues: string[]=[];
  hotfixHeaders: any = [];
  hotfixRows: any=[];
  releaseText: string ='';
  releaseValues: string[]=[];
  releaseHeaders: any = [];
  releaseRows: any=[];
	selectedValue: any = '';
	list: any =[]
	constructor(private http: HttpClient, private activatedRouter: ActivatedRoute, private router: Router, private dataService:DataService){
    this.activatedRouter.params.subscribe(params=> 
      this.queryId = params.id
      )
      console.log(this.queryId);
      this.router.routeReuseStrategy.shouldReuseRoute = function() {
        return false;
      };
  }
	ngOnInit(): void {
		
		this.getProjects();
		this.getReadMeContent();
		this.getIndexContent();
		this.getReleaseContent();
		this.getHotfixContent();
	}
	readme():boolean
	{	
		if(!this.selectedValue.readme_url)
			return true;
		else
			return false;
		
	}
	getReadMeContent()
	{
		
		this.http.get(`https://gitlab.com/api/v4/projects/${this.queryId}/repository/files/${environment.readmePath}?ref=main&${environment.privateToken}`)
		.subscribe((data:any) =>
		{
			this.readmeText = data.content;
			
			this.readmeText = atob(this.readmeText);
			
		})
		/* if(!this.readme())
		{
		let url = this.selectedValue.readme_url;
		url = url.replace("blob", "raw");
		url = url + '?private_token=glpat-Z_sheX8m8BHzHZG8sLBV'
		console.log(url);
		return url
		}
		else
			return ''; */
	}
	getIndexContent()
	{
		
		this.http.get(`https://gitlab.com/api/v4/projects/${this.queryId}/repository/files/${environment.indexdeliveriesPath}?ref=main&${environment.privateToken}`)
		.subscribe((data:any) =>
		{

			this.delError = ""		
			this.indexText = data.content;
			this.indexText = atob(this.indexText);
			this.indexValues = this.indexText.split("\n");
			this.indexHeaders = this.indexValues[1];
			this.indexRows = this.indexValues[0];
			this.indexHeaders = this.indexHeaders.split(",");
			this.indexRows = this.indexRows.split(",");
			this.indexRows.forEach((element: string, index:number) => {
				this.indexRows[index] = element.replace(/"/g, "");	
			});
		},
		
			(error) => 	{this.delError = "Liste des deliveries introuvable !"})
	}
	getHotfixContent()
	{
		
		this.http.get(`https://gitlab.com/api/v4/projects/${this.queryId}/repository/files/${environment.indexhotfixesPath}?ref=main&${environment.privateToken}`)
		.subscribe((data:any) =>
		{

			this.hotfixError = ""		
			this.hotfixText = data.content;
			this.hotfixText = atob(this.hotfixText);
			this.hotfixValues = this.hotfixText.split("\n");
			this.hotfixHeaders = this.hotfixValues[2];
			this.hotfixRows = this.hotfixValues[0];
			this.hotfixHeaders = this.hotfixHeaders.split(",");
			this.hotfixRows = this.hotfixRows.split(",");
			this.hotfixRows.forEach((element: string, index:number) => {
				this.hotfixRows[index] = element.replace(/"/g, "");	
			});
		},
		
			(error) => 	{this.hotfixError = "Liste des hotfixes introuvable !"})
	}
	getReleaseContent()
	{
		
		this.http.get(`https://gitlab.com/api/v4/projects/${this.queryId}/repository/files/${environment.indexreleasesPath}?ref=main&${environment.privateToken}`)
		.subscribe((data:any) =>
		{

			this.relError = ""		
			this.releaseText = data.content;
			
			this.releaseText = atob(this.releaseText);
			this.releaseValues = this.releaseText.split("\n");
			this.releaseHeaders = this.releaseValues[2];
			this.releaseRows = this.releaseValues[0];
			this.releaseHeaders = this.releaseHeaders.split(",");
			this.releaseRows = this.releaseRows.split(",");
			this.releaseRows.forEach((element: string, index:number) => {
				this.releaseRows[index] = element.replace(/"/g, "");	
			});
		},
		
			(error) => 	{this.relError = "Liste des releases introuvable !"})
	}
	


	ngAfterViewInit() { }
	getProjects()
	{
     
		this.http.get(this.dataService.getProjectsEndpoint(this.queryId)).subscribe((data:any) =>{
			this.selectedValue= data;
			console.log(data.description)
			if (data.description == null)
				{
					this.selectedValue.description = "non rensigné";
					console.log(this.selectedValue.description)
				}
		})
	}


}
