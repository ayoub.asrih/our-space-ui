import { Routes } from '@angular/router';
import { AccessesComponent } from './accesses/accesses.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { DocumentsComponent } from './documents/documents.component';
import { GroupDetailComponent } from './group-detail/group-detail.component';

import { FullComponent } from './layouts/full/full.component';
import { DataService } from './layouts/full/sidebar/data.service';
import { LienUtileComponent } from './lien-utile/lien-utile.component';
import { LivraisonComponent } from './livraison/livraison.component';
import { ProjectDetailComponent } from './project-detail/project-detail.component';
import { ToolsComponent } from './tools/tools.component';



export const AppRoutes: Routes = [
  
  {
    path: '',
    component: FullComponent,
    children: [
      {
        path: '',
        redirectTo: '/dashboard',
        pathMatch: 'full'
      },
      {
        path: 'project/:id', component: ProjectDetailComponent
      },
      {
        path: 'group/:id', component: GroupDetailComponent
      },
      {
        path: 'accesses', component: AccessesComponent
      },
      {
        path: 'Apps', component: LienUtileComponent
      },
      {
        path: 'tools', component: ToolsComponent
      },
      {
        path: 'documents', component: DocumentsComponent
      },
      {
        path: 'livraison', component: LivraisonComponent
      },
      {
        path: '',
        loadChildren:
          () => import('./material-component/material.module').then(m => m.MaterialComponentsModule)
      },
      
      {
        path: 'dashboard',
        loadChildren: () => import('./dashboard/dashboard.module').then(m => m.DashboardModule)
      }
    ]
  }
];
