import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { environment } from 'environments/environment';
@Component({
  selector: 'app-tools',
  templateUrl: './tools.component.html',
  styleUrls: ['./tools.component.css']
})
export class ToolsComponent implements OnInit {
  panelOpenState = true;
  tools: any = []
  constructor(private http: HttpClient) { }
  getUrls()
  {
    this.http.get(environment.toolsPath).subscribe((res:any) =>{
      this.tools = res;
    }

    )
  }
  ngOnInit(): void {
    this.getUrls();
  }

}
