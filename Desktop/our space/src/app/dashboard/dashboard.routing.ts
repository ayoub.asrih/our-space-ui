import { Routes } from '@angular/router';
import { GroupDetailComponent } from 'app/group-detail/group-detail.component';

import { DashboardComponent } from './dashboard.component';

export const DashboardRoutes: Routes = [{
  path: '',
  component: DashboardComponent
},

];
