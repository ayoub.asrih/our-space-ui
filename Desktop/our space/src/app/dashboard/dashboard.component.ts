import { HttpClient } from '@angular/common/http';
import { Component, AfterViewInit, OnInit } from '@angular/core';


const DATA: any[] = [
	{
		Name: 'ANIMATIONS',
		Children: [
			{ Name: 'ease-in' },
			{ Name: 'ease-out' },
			{ Name: 'ease-in-out' },
			{ Name: 'bounce-in' },
			{ Name: 'fade-in' },
			{ Name: 'flip-in' },
			{ Name: 'puff-in' },
			{ Name: 'roll-in' },
			{ Name: 'rotate-in' },
			{ Name: 'scale-in' },
			{ Name: 'slNamee-in' }
		]
	},
	{
		Name: 'ELEVATIONS',
		Children: [
			{ Name: 'igx-elevation' },
			{ Name: 'igx-elevations' }
		]
	},
	{
		Name: 'PALETTES',
		Children: [
			{ Name: 'igx-color' },
			{ Name: 'igx-contrast-color' },
			{ Name: 'igx-palette' },
			{ Name: 'default-palette' },
			{ Name: 'light-palette' },
			{ Name: 'dark-palette' }
		]
	},
	{
		Name: 'SCHEMAS',
		Children: [
			{ Name: '_dark-avatar' },
			{ Name: '_dark-badge' },
			{ Name: '_dark-button' },
			{ Name: '_dark-combo' },
			{ Name: '_dark-chip' },
			{ Name: '_light-avatar' },
			{ Name: '_light-badge' },
			{ Name: '_light-button' },
			{ Name: '_light-combo' },
			{ Name: '_light-chip' }
		]
	},
	{
		Name: 'THEMES',
		Children: [
			{ Name: 'igx-avatar-theme' },
			{ Name: 'igx-badge-theme' },
			{ Name: 'igx-button-theme' },
			{ Name: 'igx-combo-theme' }
		]
	},
	{
		Name: 'TYPOGRAPHY',
		Children: [
			{ Name: 'igx-button-typography' },
			{ Name: 'igx-chip-typography' },
			{ Name: 'igx-typography' },
			{ Name: 'igx-type-style' },
			{ Name: 'igx-type-scale' },
			{ Name: 'igx-type-scale-category' }
		]
	}
  ];

@Component({
	selector: 'app-dashboard',
	templateUrl: './dashboard.component.html'
})
export class DashboardComponent implements OnInit {
	 data = DATA;
	readmeText: any ='';
	constructor(private http: HttpClient){
		
	}
	ngOnInit(): void {
		this.getReadMeContent();
	}
	getReadMeContent()
	{
		
		this.http.get("https://gitlab.com/api/v4/projects/37965907/repository/files/README%2Emd?ref=main&private_token=glpat-Z_sheX8m8BHzHZG8sLBV")
		.subscribe((data:any) =>
		{
			this.readmeText = data.content;
			
			this.readmeText = atob(this.readmeText);
		})
	}

}
